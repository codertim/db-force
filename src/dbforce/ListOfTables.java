package dbforce;


import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.ListModel;



public class ListOfTables extends JList {
	private String dbmsName  = null;
	private String dbName    = null;
	private JFrame mainFrame = null;
	private String [] tableNames = {"sample_table1", 
		"sample_table2"
	};
	private MouseListener mouseListener = null;
	private ListModel listModel         = null;

	public ListOfTables() {
		super();
		// this.setListData(tableNames);   // for debugging only
		this.setFixedCellWidth(200);
		setupMouseListener();
	}


	// set lookup data
	public void setData(JFrame mainFrame, String dbmsName, String dbName, Object[] tables) {
		this.dbmsName = dbmsName;
		this.dbName   = dbName;
		this.setListData(tables);
	}


	// listen for double-clicks on select list to select table name
	private void setupMouseListener() {
		mouseListener = new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				DbTable dbTable = null;

				if(e.getClickCount() == 2) {
					int position = locationToIndex(e.getPoint());
					// System.out.println("##### ListOfTables##mouseClicked - Double-clicked on position=" + position);
					listModel = getModel();
					String tableName = (String) listModel.getElementAt(position);
					System.out.println("##### ListOfTables##mouseClicked - Double-clicked value=" + tableName);
					dbTable = new DbTable(mainFrame, dbmsName, dbName, tableName);
					return;
				}
			}
		};
		addMouseListener(mouseListener);
	}

}

