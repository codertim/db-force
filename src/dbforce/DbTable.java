package dbforce;


import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.List;
import java.util.ListIterator;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JScrollPane;


public class DbTable extends JFrame {
	JFrame mainFrame       = null;
	String dbmsName        = null;
	String dbName          = null;
	String tableName       = null;
	JLabel tableLabel      = null;
	JTable tableCells      = null;
	JScrollPane scrollPane = null;
	DbTableColNames dbTableColNamesPanel = null;
	JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP, JTabbedPane.WRAP_TAB_LAYOUT);   // Wrapping looks better than scrolling if less than 10 tabs

	public DbTable(JFrame mainFrame, String dbmsName, String dbName, String tableName) {
		this.mainFrame = mainFrame;
		this.dbmsName  = dbmsName;
		this.dbName    = dbName;
		this.tableName = tableName;

		// init frame	
		this.setTitle(dbmsName + " - " + dbName);
		this.setSize(600, 400);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setLayout(new BorderLayout());
		addComponentsToFrame();
		this.pack();
		this.setVisible(true);
		this.setPreferredSize(new Dimension(1000, 600));
		System.out.println("##### DbTable#(constructor) - Done in constructor.");
	}



	// count of columns user has chosen to display
	private int numColsSelected(List checkBoxes) {
		int numCols = 0;

		if(checkBoxes != null) {
			if(! checkBoxes.isEmpty()) {
				ListIterator iter = checkBoxes.listIterator();
				while(iter.hasNext()) {	
					JCheckBox currentCol = (JCheckBox) iter.next();
					System.out.println("    Current checkbox text=" + currentCol.getText());
					System.out.println("    Current checkbox isSelected=" + currentCol.isSelected());
					if(currentCol.isSelected()) {
						numCols++;	
					}
				}
			} else {
				System.out.println("No columns selected for query");
			}
		}

		return(numCols);		
	}


	private String getQueryLimit() {
		String queryLimit = null;

		queryLimit = dbTableColNamesPanel.getQueryLimit();

		return queryLimit;
	}


	private String getQueryOffset() {
		String queryOffset = null;

		queryOffset = dbTableColNamesPanel.getQueryOffset();

		return queryOffset;
	}


	private String getQueryOrder() {
		String queryOrder = null;

		queryOrder = dbTableColNamesPanel.getQueryOrder();

		return queryOrder;
	}


	// retrieve column names that are selected via checkbox
	public String[] getColumnsSelected() {
		String[] colsSelected = null;
		List checkBoxes = dbTableColNamesPanel.getCheckBoxList();

		if(checkBoxes != null) {
			if(! checkBoxes.isEmpty()) {
				// int numCols = checkBoxes.size();
				int numCols = numColsSelected(checkBoxes);
				colsSelected = new String[numCols];
				ListIterator iter = checkBoxes.listIterator();
				int selectedColCounter = 0;
				while(iter.hasNext()) {	
					JCheckBox currentCol = (JCheckBox) iter.next();
					System.out.println("    Current checkbox text=" + currentCol.getText());
					System.out.println("    Current checkbox isSelected=" + currentCol.isSelected());
					if(currentCol.isSelected()) {
						colsSelected[selectedColCounter] = currentCol.getText();
						selectedColCounter++;	
					}
				}
			} else {
				System.out.println("No columns selected for query");
			}
		}

		return(colsSelected);
	}



	// make sql db query string
	private String prepareQueryString(String[] colNames, String queryOrder, String queryLimit, String queryOffset) {
		System.out.println("##### DbTable#prepareQueryString - queryOrder="  + queryOrder);
		System.out.println("##### DbTable#prepareQueryString - queryLimit="  + queryLimit);
		System.out.println("##### DbTable#prepareQueryString - queryOffset=" + queryOffset);
		// TODO: optimize with StringBuffer	
		String query = null;

		if( (colNames != null) && (colNames.length > 0) ) {
			query = "SELECT";

			for(int i=0; i < colNames.length; i++) {
				query += " " + colNames[i];
				if(i < (colNames.length - 1)) {
					query += ",";
				}
			}

			query += " FROM ";
			query += tableName;
		}

		// order by
		if(queryOrder != null) {	
			query += " ORDER BY " + queryOrder;
		}

		// limit
		if(queryLimit != null) {	
			query += " LIMIT " + queryLimit;
		}

		// offset
		if(queryOffset != null) {	
			query += " offset " + queryOffset;
		}

		return(query);
	}



	// peform sql query and store in array
	private Object[][] processQueryResultSet(Connection conn, String queryString, int numCols, String[] colNames) throws Exception {
		Object [][] results2dArray = null;
		Object []   rowArray       = null;
		int         numRows        = 0;

		try {
			if(conn != null) {
				Statement stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
				ResultSet rset = stmt.executeQuery(queryString);

				// count rows before reading
				while(rset.next()) {
					numRows++;
				}

				results2dArray = new Object[numRows][];
				// start results at beginning to read data
				rset.beforeFirst();

				// read data from db and store in nested array
				int j = 0;
				while(rset.next()) {
					System.out.println("j = " + j);
					rowArray = new Object[numCols];
					for(int i=0; i < numCols; i++) {
						// System.out.println("     i=" + i);
						// Object colObjectValue = rset.getObject(i);
						Object colObjectValue = rset.getObject(colNames[i]);
						rowArray[i] = colObjectValue;
						System.out.println("      col object value=" + colObjectValue);
					}
					results2dArray[j] = rowArray;
					// rowArray = null;  TODO: delete me if ok
					j++;
				}

				if(j==0) {
					// no results
					results2dArray = getEmptyRowResult(colNames);
				}

				System.out.println("\n Number of rows returned = " + numRows);
				rset.close();
				stmt.close();
				conn.close();
			}
		} catch(SQLException e) {
				System.out.println("##### DbTable#processQueryResultSet - ERROR getting results: " + e.getMessage());
		}
		System.out.println("##### DbTable#processQueryResultSet - 2D results array = " + results2dArray);
		DbDebug.show2dArray(results2dArray);
		return(results2dArray);
	}



	// do query with columns selected
	public void processNewQuery() {
		Connection conn        = null;
		Object[][] data2dArray = null;
		String []  colNames    = getColumnsSelected();
		String     queryOrder  = getQueryOrder();
		String     queryLimit  = getQueryLimit();
		String     queryOffset = getQueryOffset();
		System.out.println("   Column names = " + colNames);

		String queryString = prepareQueryString(colNames, queryOrder, queryLimit, queryOffset);
		System.out.println("   Query string = " + queryString);

		if(queryString != null) {
			try {
				conn = Db.getConnection(dbmsName, dbName);
				data2dArray = processQueryResultSet(conn, queryString, colNames.length, colNames);

				if(conn != null) {
					conn.close();
				}
			} catch(Exception e) {
				System.out.println("##### ERROR: " + e.getMessage());
			}
		} else {
			// data2dArray = { {"1", "2"}, {"10", "11"} };
			data2dArray = getEmptyRowResult(colNames);
		}

		// tabbedPane.add("New Query", new JLabel("Testing"));
		JTable tableNewQueryData = new JTable(data2dArray, colNames);
		tableNewQueryData.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);   // allow scrolling to right
		JScrollPane scrollPaneNewQuery = new JScrollPane(tableNewQueryData);
		SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");	
		tabbedPane.add("Query at " + formatter.format(new Date()), scrollPaneNewQuery);
		tabbedPane.setSelectedIndex(tabbedPane.getTabCount() - 1);
	}



	// bottom panel of window for table
	private JPanel getSouthPanel() {
		JPanel southPanel = new JPanel();
		JButton goSearchButton = new JButton("Search");
		goSearchButton.addActionListener( new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				processNewQuery();	
			}
		});
		southPanel.add(goSearchButton);
		return(southPanel);
	}



	// add main GUI components to table frame
	private void addComponentsToFrame() {
		tableLabel = new JLabel("Table: " + tableName);
		tableCells = initGuiTable();

		// populate layout with components
		this.add(tableLabel, BorderLayout.NORTH);

		dbTableColNamesPanel = new DbTableColNames(dbmsName, dbName, tableName);
		this.add(dbTableColNamesPanel, BorderLayout.WEST);
		this.add(getSouthPanel(), BorderLayout.SOUTH);

		/*	TODO: move this somewhere else  */
			// this.add(tableCells.getTableHeader(), BorderLayout.PAGE_START);     // may not be nec. if using scrolllpane
			tableCells.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);   // allow scrolling to right
			scrollPane = new JScrollPane(tableCells);	
			// this.add(scrollPane, BorderLayout.CENTER);
			tabbedPane.add("Query - Sample", scrollPane);
			// tabbedPane.add("(Query - Testing)", new JLabel("Testing ... Testing ... Testing"));
			this.add(tabbedPane, BorderLayout.CENTER);
	}


	// data table presented as GUI table
	private JTable initGuiTable() {
		Object[][] data = {
			{"John", "Doe", "Santa Barbara", "CA", "93101", "805-555-5555", "123", "1234 Main St.", "Suite 111"}, 
			{"Jane", "Doe", "Ventura", "CA", "93001", "805-555-1234", "222", "555 State St.", "Suite 222"}
		};
		String colNames[] = {"First Name", "Last Name", "City", "State", "Zip", "Phone", "Ext", "Address1", "Address2"};
		JTable tableCells = new JTable(data, colNames);
		return(tableCells);
	}


	// columnn names from db table that user selected
	private String[] getColumnNames(String table) {
		String[] colNames = null;
		String query = "select * from " + table;

		try {
			Connection conn = getConnection();
			Statement stmt = conn.createStatement();
			ResultSet rset = stmt.executeQuery(query);
			ResultSetMetaData md = rset.getMetaData();
			int numCols = md.getColumnCount();
			System.out.println("##### DbTable#getColumnNames - numCols=" + numCols);
			colNames = new String[numCols];
			for(int i=0; i < numCols; i++) {
				String colName = md.getColumnName(i+1);
				System.out.println("##### DbTable#getColumnNames - colName=" + colName);
				colNames[i] = colName;
			}
			rset.close();
			stmt.close();
			conn.close();
		} catch(SQLException e) {
			System.out.println("##### DbTable#getColumnNames - Error: " + e.getMessage());
		}

		return colNames;
	}



	// standard db connection
	private Connection getConnection() {
		Connection conn = null;
		Properties connectionProps = new Properties();
		Properties fileProps = Db.getFileProperties();

		try {

			if("postgresql".equals(dbmsName)) {
				connectionProps.put("user", "postgres");
				connectionProps.put("password", "");
				conn = DriverManager.getConnection("jdbc:postgresql:" + dbName);
				System.out.println("Retrieved connection for: " + dbmsName);
			} else if("mysql".equals(dbmsName)) {
				conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/" + dbName, fileProps.getProperty("username", "default"), fileProps.getProperty("password", "default"));
				System.out.println("Retrieved connection for: " + dbmsName);
			}
		} catch(SQLException e) {
			System.out.println("Exception=" + e.getMessage());
			// throw e;  TODO: rethrow
		}

		return(conn);
	}


	// create empty data array because no results were found in database table
	private static Object[][] getEmptyRowResult(String [] colNames)  {
		Object[][] emptyData2dArray = null;
		String [] rowArrayNoData = new String[colNames.length];
		for(int i=0; i < rowArrayNoData.length; i++ ) {
			rowArrayNoData[i] = "";
		}
		emptyData2dArray = new Object[1][colNames.length];
		emptyData2dArray[0] = rowArrayNoData;

		return(emptyData2dArray);
	}

}

