package dbforce;


import java.awt.Color;
import java.awt.Component;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;



public class DbmsTableCellRenderer extends JLabel implements ListCellRenderer {
	Color colorWhenSelected = new Color(0, 0, 128);
	ImageIcon iconMysql    = new ImageIcon("dbforce/icons/mysql26x26.gif");
	ImageIcon iconPostgres = new ImageIcon("dbforce/icons/postgres26x26.gif");
	String dbmsNames[]     = null;     // e.g. {"mysql", "postgresql"};

	public DbmsTableCellRenderer(String[] dbms) {
		setOpaque(true);
		dbmsNames = dbms;
	}


	public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
		if(index == -1) {
			int selected = list.getSelectedIndex();	
			if(selected == -1)
				return this;
			else
				index = selected;
		}

		String dbmsName = dbmsNames[index];
		setText(" " + dbmsName + " ");

		if(dbmsName.indexOf("mysql") > -1) {
			setIcon(iconMysql);
		} else if(dbmsName.indexOf("postgres") > -1) {
			setIcon(iconPostgres);
		}  else {
			setIcon(null);
		}

		if(isSelected) {
			setBackground(colorWhenSelected);
			setForeground(Color.white);
		} else {
			setBackground(Color.white);
			setForeground(Color.black);
		}
		// setSize(100, 700);  seems  to have no affect
		return this;
	}


}

