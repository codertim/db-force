
package dbforce;


import java.awt.Dimension;
import java.awt.GridLayout;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextField;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;


class DbTableColNames extends JPanel {
	private List<JCheckBox> checkBoxList = new ArrayList<JCheckBox>();
	private JPanel      checkBoxPanel   = new JPanel();
	private JComboBox   orderComboBox   = null;
	private JTextField  limitTextField  = null;
	private JTextField  offsetTextField = null;
	private String[]    colNames        = null;
	private String      dbmsName        = null;
	private String      dbName          = null;
	private JScrollPane scrollPane      = null;
	private String      tableName       = null;
	private final int   PANEL_WIDTH     = 200;

	public DbTableColNames(String dbmsName, String dbName, String tableName) {
		this.dbmsName  = dbmsName;
		this.dbName    = dbName;
		this.tableName = tableName;

		// this.setLayout(new GridLayout(0, 1));
		this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		checkBoxPanel.setLayout(new BoxLayout(checkBoxPanel, BoxLayout.PAGE_AXIS));
		addComponents();
	}


	@Override
	public int getWidth() {
		return(PANEL_WIDTH);
	}


	public List getCheckBoxList() {
		return(checkBoxList);
	}


	public String getQueryLimit() {
		String queryLimit = null;

		String limitInput = limitTextField.getText();
		if( (limitInput != null) && (limitInput.trim().length() > 0) ) {
			queryLimit = limitInput.trim();
		}

		System.out.println("##### DbTableColNames#getQueryLimit - query limit = " + queryLimit);

		return(queryLimit);
	}


	public String getQueryOffset() {
		String queryOffset = null;

		String offsetInput = offsetTextField.getText();
		if( (offsetInput != null) && (offsetInput.trim().length() > 0) ) {
			queryOffset = offsetInput.trim();
		}

		System.out.println("##### DbTableColNames#getQueryOffset - query offset = " + queryOffset);

		return(queryOffset);
	}


	public String getQueryOrder() {
		String queryOrder = null;

		String inputOrder = (String) orderComboBox.getSelectedItem();
		if( (inputOrder != null) && (inputOrder.trim().length() > 0) ) {
			queryOrder = inputOrder.trim();
		}

		System.out.println("##### DbTableColNames#getQueryOrder - query order = " + queryOrder);

		return queryOrder;
	}


	// add GUI components to this column name panel
	private void addComponents() {
		System.out.println("##### DbTableColNames#addComponents - Starting ...");
		this.add(new JSeparator());
		this.add(new JSeparator());

		colNames = addColNames();

		// query order
		JPanel orderPanel = new JPanel();
		JLabel orderLabel = new JLabel("Order");
		orderPanel.add(orderLabel);
		orderComboBox = new JComboBox(colNames);
		orderPanel.add(orderComboBox);
		this.add(orderPanel);

		JPanel limitPanel = new JPanel();
		JLabel limitLabel = new JLabel("Limit");
		limitTextField = new JTextField(5);
		limitPanel.add(limitLabel);
		limitPanel.add(limitTextField);
		this.add(limitPanel);

		JPanel offsetPanel = new JPanel();
		JLabel offsetLabel = new JLabel("Offset");
		offsetTextField = new JTextField(5);
		offsetPanel.add(offsetLabel);
		offsetPanel.add(offsetTextField);
		this.add(offsetPanel);

		this.add(new JSeparator());

		// column names
		this.add(new JLabel("Column Names:"));
		scrollPane = new JScrollPane(checkBoxPanel);
		scrollPane.setPreferredSize(new Dimension((PANEL_WIDTH-20), 400));   // width and height of left scrollpane with column names
		this.add(scrollPane);
	}


	// show column names for a table so user can select a subset
	private String[] addColNames() {
		String[] columnNames = null;

		try {
			Connection conn = getConnection();
			// Connection conn = Db.getConnection();
			DatabaseMetaData metaData = conn.getMetaData();
			ResultSet rset = metaData.getColumns(null, null, tableName, null);
			List<String> colList = new ArrayList<String>();

			while(rset.next()) {
				String colName = rset.getString("COLUMN_NAME");
				System.out.println("  Current column name: " + colName);
				colList.add(colName);
				JCheckBox currentCheckBox = new JCheckBox(colName);
				currentCheckBox.setSelected(true);
				checkBoxList.add(currentCheckBox);
				checkBoxPanel.add(currentCheckBox);
			}
			columnNames = new String[colList.size()];
			colList.toArray(columnNames);
			rset.close();
			conn.close();
		} catch(Exception e) {
			System.out.println("##### DbTableColNames#addColNames - Error:" + e.getMessage());
		}

		return(columnNames);
	}



	// standard db connection
	private Connection getConnection() {
		Connection conn = null;
		Properties connectionProps = new Properties();
		Properties fileProps = Db.getFileProperties();

		try {

			if("postgresql".equals(dbmsName)) {
				connectionProps.put("user", "postgres");
				connectionProps.put("password", "");
				conn = DriverManager.getConnection("jdbc:postgresql:" + dbName, connectionProps);
				System.out.println("Retrieved connection for: " + dbmsName);
			} else if("mysql".equals(dbmsName)) {
				conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/" + dbName, fileProps.getProperty("username", "default"), fileProps.getProperty("password", "default"));
				System.out.println("Retrieved connection for: " + dbmsName);
			}
		} catch(SQLException e) {
			System.out.println("Exception=" + e.getMessage());
			// throw e;  TODO: rethrow
		}

		return(conn);
	}

}

