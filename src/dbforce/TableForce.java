package dbforce;


import java.awt.Color; 

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Properties;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;


class TableForce extends JPanel 
		 implements ActionListener {

	private Connection conn             = null;
	private DatabaseMetaData dbMetaData = null;
	private JComboBox dbList            = null;
	private static JFrame mainFrame     = null;
	private JTextField dbNameText       = null;
	private ListOfTables listOfTables   = new ListOfTables();
	private static Color seaGreen       = new Color(75, 155, 111);   // 109, 174, 137);


	public TableForce() {
		JLabel dbLabel = new JLabel("Choose database type: ");
		JButton goButton = new JButton("Go");
		goButton.addActionListener(this);
		goButton.setToolTipText("Press this to continue ...");
		goButton.setActionCommand("go");

		// dbms
		String []dbms = {"Select a DBMS ...", "mysql", "postgresql"};
		// dbList = new JList(dbms);
		dbList = new JComboBox(dbms);
		// dbList.setVisibleRowCount(1);

		// database
		JLabel dbNameLabel    = new JLabel("DB Name: ");
		dbNameText = new JTextField(getPropsDbName(), 10);

		// tables
		JLabel tableNameLabel    = new JLabel("Tables: ");
		JScrollPane tableScrollPane = new JScrollPane(listOfTables);

		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

		// line 1
		JPanel panelLine1 = new JPanel();
		panelLine1.add(dbLabel);
		panelLine1.add(dbList);
		// dbList.setCellRenderer(new DbmsTableCellRenderer(dbms));
		dbList.setRenderer(new DbmsTableCellRenderer(dbms));

		// line 2
		JPanel panelLine2 = new JPanel();
		panelLine2.add(dbNameLabel);
		panelLine2.add(dbNameText);
		panelLine2.add(goButton);

		// line 3
		JPanel panelLine3 = new JPanel();
		add(new JSeparator(SwingConstants.HORIZONTAL));
		panelLine3.add(tableNameLabel);
		panelLine3.add(tableScrollPane);

		panelLine1.setBackground(seaGreen);
		panelLine2.setBackground(seaGreen);
		panelLine3.setBackground(seaGreen);

		add(panelLine1);
		add(panelLine2);
		add(panelLine3);
	}


	// db info from file properties
	private String getPropsDbName() {
		String propsDbName = null;
		FileInputStream fis = null;
		try {
			fis = new FileInputStream("src/dbforce/dbforce.properties");
		} catch(FileNotFoundException e) { System.out.println("Property file not found=" + e.getMessage()); }
		Properties fileProps = new Properties();
		try {
			fileProps.load(fis);
		} catch(IOException e) { System.out.println("Property file not loaded=" + e.getMessage()); }
		propsDbName = fileProps.getProperty("dbname", "");
		return(propsDbName);
	}


	// @Override window method
	public int getWidth() { return 600; }




	// retrieve list of tables from db info
	public void actionPerformed(ActionEvent e) {
		ArrayList tableList = null;

		if("go".equals(e.getActionCommand())) {

		// tableList = Db.testConnection((String)dbList.getSelectedValue(), dbNameText.getText());   // JList version
			String dbms = (String)dbList.getSelectedItem();
			if( (dbms == null) || (dbms.equals(""))  || (dbms.indexOf("Select") > -1) ) {
				JOptionPane.showMessageDialog(mainFrame, "Please select a DBMS (e.g. mysql)", "DBMS Required", JOptionPane.INFORMATION_MESSAGE);
				return;
			}

			String dbName = dbNameText.getText();
			try {
				tableList = Db.testConnection(dbms, dbName);   // JComboBox version
			} catch(Exception te) {
				System.err.println("##### actionPerformed - Error: " + te.getMessage());
				JOptionPane.showMessageDialog(mainFrame, "Error - " + te.getMessage(), "Problem Getting Table Information", JOptionPane.INFORMATION_MESSAGE);
			}

			if(tableList != null)
				System.out.println("tableList=" + tableList.toString());
			listOfTables.clearSelection();

			if(tableList != null) {
				// listOfTables.setListData(tableList.toArray());
				listOfTables.setData(mainFrame, dbms, dbName, tableList.toArray());
			}
		}
	}


	// initialize main GUI
	private static void startGUI() {
		// setup
		mainFrame = new JFrame("Table Force");
		mainFrame.setSize(700, 500);
		mainFrame.setBackground(seaGreen);
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// setup window contents
		TableForce contentPane = new TableForce();
		// contentPane.setOpaque(true);
		mainFrame.setContentPane(contentPane);
		contentPane.setBackground(seaGreen);

		// show to user
		// mainFrame.pack();
		mainFrame.setVisible(true);
	}



	public static void main(String [] args) {
		System.out.println("Starting ...");

		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				startGUI();
			}
		});

		System.out.println("Done.");
	}
}


