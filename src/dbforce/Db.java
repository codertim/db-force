package dbforce;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.IOException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Properties;


public class Db {

	// custom properties
	public static Properties getFileProperties() {
		Properties  fileProperties = new Properties();
		String      propsFilename  = "dbforce_dbforce_properties";
		InputStream iStream        = null;

		try {
			// set db properties from custom file if it exists
			iStream = new FileInputStream(propsFilename);
		} catch(FileNotFoundException e) { System.out.println("Property file not found"); }

		if(iStream != null) {
			try {
				fileProperties.load(iStream);
			} catch(IOException e) { }
		} else {
			System.out.println("INFO: properties file not found - " + propsFilename);
		}

		return(fileProperties);
	}



	public static Connection getConnection(String dbmsName, String dbName) throws Exception {
		Connection conn = null;
		Properties fileProps = getFileProperties();
		Properties connectionProps = new Properties();

		try {
			if("postgresql".equals(dbmsName)) {
				connectionProps.put("user", "postgres");
				connectionProps.put("password", "");
				conn = DriverManager.getConnection("jdbc:postgresql:" + dbName, connectionProps);
				System.out.println("Retrieved connection for: " + dbmsName);
			} else if("mysql".equals(dbmsName)) {
				conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/" + dbName, fileProps.getProperty("username", "default"), fileProps.getProperty("password", "default"));
				System.out.println("Retrieved connection for: " + dbmsName);
			}
		} catch(SQLException e) {
			System.out.println("Exception=" + e.getMessage());
			throw e;
		}

		return(conn);
	}



	// check if db credentials ok
	public static ArrayList testConnection(String dbmsName, String dbName) throws Exception {
		java.sql.DatabaseMetaData dbMetaData = null;
		Connection conn                      = null;
		ArrayList<String> tableList          = new ArrayList<String>();

		Properties fileProps       = getFileProperties();
		Properties connectionProps = new Properties();

		/*
		try {
			Class.forName("org.postgresql.Driver");   // Needed prior to JDBC 4
		} catch(ClassNotFoundException e) {
			System.out.println("Exception forName=" + e.getMessage());
		}
		*/

		try {

			if("postgresql".equals(dbmsName)) {
				connectionProps.put("user", "postgres");
				connectionProps.put("password", "");
				// conn = DriverManager.getConnection("jdbc:postgresql:dbName");
				conn = DriverManager.getConnection("jdbc:postgresql:" + dbName, connectionProps);
				// conn = DriverManager.getConnection("jdbc:" + dbmsName +
				//          "://" + "localhost" + ":" + "5432" + "/mydb", connectionProps);
				System.out.println("Retrieved connection for: " + dbmsName);
			} else if("mysql".equals(dbmsName)) {
				// conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/mysql", connectionProps);
				conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/" + dbName, fileProps.getProperty("username", "default"), fileProps.getProperty("password", "default"));
				System.out.println("Retrieved connection for: " + dbmsName);
			}
		} catch(SQLException e) {
			System.out.println("Exception=" + e.getMessage());
			throw e;
		}

		// Now release connection
		if(conn != null) {
			try {
				dbMetaData = conn.getMetaData();
				String[] types = {"TABLE"};
				ResultSet rset = dbMetaData.getTables(null, null, "%", types);

				while(rset.next()) {
					String tableName    = rset.getString(3);
					String tableCatalog = rset.getString(1);
					String tableSchema  = rset.getString(2);
					System.out.println("**********");
					System.out.println("  Table name="    + tableName);
					System.out.println("  Table catalog=" + tableCatalog);
					System.out.println("  Table schema="  + tableSchema);
					tableList.add(tableName);
				}
			} catch(SQLException e) { 
				System.out.println("Error getting meta data: " + e.getMessage()); 
			throw e;
			}

			try {
				System.out.println("Closing connection ...");
				conn.close();
				System.out.println("Closed connection.");
			} catch(SQLException e) { 
				System.out.println("Retrieved connection for: " + dbmsName); 
				throw e;
			}
		}

		return tableList;
	}
}

