package dbforce;

public class DbDebug {

	// show values of nested 2d array for debugging purposes
	public static void show2dArray(Object [][] array2d) {
		if(array2d != null) {
			System.out.println("\n\n\nDbDebug#show2dArray - Row count = " + array2d.length);
			Object[] firstInnerArray = array2d[0];
			System.out.println("DbDebug#show2dArray - Col count = " + firstInnerArray.length);
			for(int i=0; i < array2d.length; i++) {
				Object[] innerArray = array2d[i];
				System.out.println("\n\n***** Row: *****");
				System.out.print("  ");
				if(innerArray != null) {
					for(int j=0; j < innerArray.length; j++) {
						System.out.print(innerArray[j]);
						System.out.print("|");
					}
				} else {
					System.out.print("  DbDebug#show2dArray - Inner array is null for index = " + i);
				}
			}
		} else {
			System.out.println("\n\n\nDbDebug#show2dArray - Cannot show 2d array because it is null!");
		}
	}
}

